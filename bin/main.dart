import 'dart:io';
import "../lib/async_future_basic.dart" as dart_future;
import "../lib/async_stream_basic.dart" as dart_stream;
import '../lib/http_server_basic.dart' as dart_server;
import '../lib/http_client_basic.dart' as dart_client;

const main_menu = "\n---------- Dart Basic ---------- \n" +
    "1. Dart Future Example \n" +
    "2. Dart Stream Example \n" +
    "3. Dart HTTP Server \n" +
    "4. Dart HTTP Client \n" +
    "Type any letter for exit \n" +
    "--------------------------------- \n\n";

main() async {
  bool isExit = false;
  while (!isExit) {
    stdout.write(main_menu);

    try {
      int input = int.parse(stdin.readLineSync().toLowerCase());
      if (input == 1)
        await dart_future.action();
      else if (input == 2)
        await dart_stream.action();
      else if (input == 3)
        await dart_server.listen();
      else if (input == 4) await dart_client.postAction();
    } catch (ex) {
      stdout.write("Bye");
      isExit = true;
    }
  }
  exit(0);
}
