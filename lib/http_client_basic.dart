import 'dart:io';
import 'package:basic_utils/basic_utils.dart';
import 'package:http/http.dart';

String host = InternetAddress.loopbackIPv4.host;

postAction() async {
// Define some headers and query parameters
  Map<String, String> headers = {"Accept": "application/json"};
  Map<String, String> queryParameters = {"foo": "bar", "key": "chain"};

// Body
  String body = "{ 'some':'json'}";

// Send request
  Response responsePOSTData = await HttpUtils.postForFullResponse(
      "http://localhost:4040", body,
      headers: headers, queryParameters: queryParameters);

  Response responseGETData = await HttpUtils.getForFullResponse(
      "http://localhost:4040",
      headers: headers,
      queryParameters: queryParameters);

  print(
      "Response \n ${responsePOSTData.statusCode} -> ${responsePOSTData.body} \n " +
          "${responseGETData.statusCode} -> ${responseGETData.body}");
}
