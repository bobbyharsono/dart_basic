import 'dart:io';
import 'dart:convert';

listen() async {
  // define server
  var server = await HttpServer.bind(InternetAddress.loopbackIPv4, 4040);
  print("Listening to ${server.port}");

  await for (HttpRequest request in server) {
    await handleRequest(request);
    request.response.close();
  }
}

handleRequest(HttpRequest request) async {
  if (request.method == 'GET')
    await handleGET(request);
  else if (request.method == 'POST')
    await handlePOST(request);
  else {
    await request.response
      ..statusCode = HttpStatus.methodNotAllowed
      ..write("Unsupported request method ${request.method}")
      ..close();
  }
}

handleGET(HttpRequest request) async {
  print(
      "Server getting request GET from ${request.connectionInfo.remoteAddress.address}");
  var extractedInfo = request.uri.queryParameters['foo'];
  var content = await request.transform(Utf8Decoder()).join();

  request.response
    ..statusCode = HttpStatus.ok
    ..write(
        "You sent information ${extractedInfo} and payload ${content} with method ${request.method}")
    ..close();
}

handlePOST(HttpRequest request) async {
  print(
      "Server getting request POST from ${request.connectionInfo.remoteAddress.address}");
  var extractedInfo = request.uri.queryParameters['key'];
  var content = await request.transform(Utf8Decoder()).join();

  request.response
    ..statusCode = HttpStatus.ok
    ..write(
        "You sent information ${extractedInfo} and payload ${content} with method ${request.method}")
    ..close();
}
