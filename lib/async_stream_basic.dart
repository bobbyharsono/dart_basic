import 'dart:async' show Future, Stream;

Future<int> sumStream(Stream<int> stream) async {
  var sum = 0;

  try {
    await for (var value in stream) {
      sum += value;
    }
  } catch (e) {
    return -1;
  }
  return sum;
}

Stream<int> countStream(int to) async* {
  for (int i = 1; i <= to; i++) {
    // kinda like return but not terminating function
    yield i;
  }
}

action() async {
  var stream = countStream(10);
  var sum = await sumStream(stream);
  print(sum); // 55
}
