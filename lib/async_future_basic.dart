import 'dart:async' show Future;
import 'dart:async';
import 'dart:io';
import 'dart:convert';

// final const can be populated once, while const is not
final duration = Duration(seconds: 1);
final news = "<null>";

action() async{
  //printDailyNewsDigest().whenComplete(() => exit(0));
  await printDailyNewsDigest();
  printWinningLotteryNumbers();
  printWeatherForecast();
  printBaseballScore();
}

Future printDailyNewsDigest() async {
  var newsDigest = await gatherNewsReportsFromServer();
  await print(newsDigest);
}

Future<String> gatherNewsReports() => Future.delayed(duration, () => news);

Future gatherNewsReportsFromServer() async {
  var retval;
  var request = await HttpClient().getUrl(Uri.parse('https://www.dartlang.org/f/dailyNewsDigest.txt'));
  var response = await request.close();
  await for (var contents in response.transform(Utf8Decoder())) {
    retval = contents;
  }
  return retval;
}

printWinningLotteryNumbers() {
  print('Winning lotto numbers: [23, 63, 87, 26, 2]');
}

printWeatherForecast() {
  print("Tomorrow's forecast: 70F, sunny.");
}

printBaseballScore() {
  print('Baseball score: Red Sox 10, Yankees 0');
}
